require 'rails_helper'

RSpec.describe Project, type: :model do
  before(:each) do
    @user = FactoryGirl.create(:user)
  end

  context 'registered user' do
    it 'could create a correct ToDo List' do
      Project.create(name: 'Some Name', user: @user)
      expect(Project.count).to eq(1)
    end

    it 'could not create a ToDo List without user_id' do
      Project.create(name: 'Some Name', user: nil)
      expect(Project.count).to eq(0)
    end

    it 'could not create a ToDo List without project name' do
      Project.create(name: '', user: @user)
      expect(Project.count).to eq(0)
    end

    it 'could not create a ToDo List with a duplicated name' do
      Project.create(name: 'Some Name', user: @user)
      Project.create(name: 'Some Name', user: @user)
      expect(Project.count).to eq(1)
    end
  end
end
