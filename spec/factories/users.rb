FactoryGirl.define do
  factory :user do
    email 'test@example.com'
    password 'thepassword'
    password_confirmation 'thepassword'
  end

end
