Given(/^I am logged in$/) do
  step %{I am on the homepage}
  step %{I have a User with email "test@example.com" and password "thepassword"}
  step %{I click "Login"}
  step %{I login with email "test@example.com" and password "thepassword"}
end

Given(/^I open the New Project page$/) do
  visit app_path + '/projects/new'
end

Given(/^I create "([^"]*)" project$/) do |project_name|
  step %{I click "Add TODO List"}
  step %{I fill in "Project Name" with "#{project_name}"}
  step %{I press "Save"}
end


Given(/^Other user create "([^"]*)" project$/) do |project_name|
  user2 = FactoryGirl.create(:user,
                             email: 'anotherguy@example.com'
  )
  FactoryGirl.create(:project,
                     name: project_name,
                     user: user2
  )
end


When(/^I update project as "([^"]*)"$/) do |project_name|
  step %{I use "edit"}
  step %{I fill in "Project Name" with "#{project_name}"}
  step %{I press "Save"}
end

When(/^I delete a project$/) do
  step %{I use "delete"}
end

Then(/^There should be "([^"]*)" Projects$/) do |count|
  expect(Project.count).to eq(count.to_i)
end
