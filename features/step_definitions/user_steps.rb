Given(/^I am on the homepage$/) do
  visit root_path
end

Given(/^I create a User with email "([^"]*)" and password "([^"]*)"$/) do |email, password|
  step %{I fill in "Email" with "#{email}"}
  step %{I fill in "Password" with "#{password}"}
  step %{I fill in "Password confirmation" with "#{password}"}
  step %{I press "Sign up"}
  step %{I should see "Welcome! You have signed up successfully."}
end

Given(/^I login with email "([^"]*)" and password "([^"]*)"$/) do |email, password|
  step %{I fill in "Email" with "#{email}"}
  step %{I fill in "Password" with "#{password}"}
  step %{I press "Log in"}
  step %{I should see "Signed in successfully."}
end

Given(/^I have a User with email "([^"]*)" and password "([^"]*)"$/) do |email, password|
  FactoryGirl.create(:user,
    email: email,
    password: password,
    password_confirmation: password
  )
end

Given (/^I click "([^"]*)"$/) do |link_name|
  find_link(link_name).click
end

When(/^I press "([^"]*)"/) do |button|
  find_button(button).click
end

When(/^I use "([^"]*)"/) do |widget_class|
  find(".#{widget_class}").click
end

When(/^I fill in "([^"]*)" with "([^"]*)"$/) do |field, value|
  fill_in field, with: value
end

Then(/^I should see "([^"]*)"$/) do |text|
  expect(page).to have_content(text)
end

Then /^I should not see "([^"]*)"$/ do |text|
  expect(page).to have_no_content(text)
end

Then /^(?:|I )should be on app page$/ do
  current_path = URI.parse(current_url).path
  expect(current_path).to eq(app_path)
end