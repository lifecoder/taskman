@javascript
Feature: User works with his ToDo Lists

  Background:
    Given I am logged in

  Scenario: I see the App page
    Then I should see "Add TODO List"

  Scenario: I proceed to an inner page directly
    Given I open the New Project page
    Then I should see "Add Project"

  Scenario: I create a new project
    Given I create "My Test Project" project
    Then I should see "My Test Project"
    And I should see "Project created"

  Scenario: I update a project
    Given I create "My Test Project" project
    When I update project as "My Updated Project"
    Then I should see "My Updated Project"
    And I should see "Project updated"

  Scenario: I remove project
    Given I create "My Test Project" project
    When I delete a project
    Then There should be "0" Projects

  Scenario: I try to create a project with an empty name
    Given I create "" project
    Then I should see "Add Project"
    And There should be "0" Projects

  Scenario: I try to create a project with duplicated name
    Given I create "My Test Project" project
    And I create "My Test Project" project
    Then I should see "Name should be unique"

  Scenario: I should see only own ToDo projects
    Given Other user create "Other Test Project" project
    And I create "My Test Project" project
    Then I should see "My Test Project"
    And I should not see "Other Test Project"