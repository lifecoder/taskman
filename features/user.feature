Feature: User opens the site

  Background:
    Given I am on the homepage

  Scenario: I open the site as a Guest
    Then I should see "Login"
    And I should see "Register"
    And I should see "Sign in with Facebook"

  Scenario: User wants to sign up
    Given I click "Register"
    And I create a User with email "test@example.com" and password "thepassword"

  Scenario: User wants to log in
    Given I have a User with email "test@example.com" and password "thepassword"
    And I click "Login"
    And I login with email "test@example.com" and password "thepassword"
    Then I should be on app page
