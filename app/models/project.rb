class Project < ActiveRecord::Base
  belongs_to :user

  validates :name, :user_id, presence: true
  validates :name, uniqueness: {
                     case_sensitive: false,
                     scope: :user_id,
                     message: I18n.t('project.validation.name.duplicated')
                 }
end
