"use strict";

var app = angular.module('taskman', [
  'ngResource', 'ngRoute', // 'ui'
  'tmDashboard', 'tmProjects'
]);

app.config(function($locationProvider, $routeProvider) {
    $locationProvider.html5Mode(true);
    $routeProvider
        .when('/dashboard', {
            templateUrl: '/assets/dashboard',
            controller: 'DashboardController'
        })
        .when('/projects/new', {
            templateUrl: '/assets/projects/form',
            controller: 'ProjectEditController'
        })
        .when('/projects/:id', {
            templateUrl: '/assets/projects/form',
            controller: 'ProjectEditController'
        })

        .otherwise({ redirectTo: '/dashboard' });
});

// INTERCEPTOR
// common errors handling
app.factory('tmErrorInterceptor', function() {
    var sbErrorInterceptor = {
        responseError: function(response) {
            // ignore non-API calls
            if (response.config.url.indexOf('/api') !== 0) { return response; }

            if (response.status != 200 && typeof(response.data.errors) != 'undefined') {
                $.each(response.data.errors, function(k, message) {
                    tmMessage.error(message);
                });
                return response;
            }
            return response;
        }
    };

    return sbErrorInterceptor;
});

app.config(['$httpProvider', function($httpProvider) {
    $httpProvider.interceptors.push('tmErrorInterceptor');
}]);