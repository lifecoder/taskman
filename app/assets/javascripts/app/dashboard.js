"use strict";

var tmDashboard = angular.module('tmDashboard', ['ngResource']);

tmDashboard.controller('DashboardController', function($scope, Project) {
    $scope.projects = Project.query({}, function(result) {
        $scope.projects = result;
    });

    $scope.remove = function(item) {
        // remove from server
        Project.delete({id: item.id}, function(response){
            var index = $scope.projects.indexOf(item);
            $scope.projects.splice(index, 1);
            tmMessage.success('Project "' + item.name + '" removed successfully');
        });
    }
});