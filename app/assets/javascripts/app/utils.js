"use strict";

/**
 * File: contains general non-angular resources
 */


// notifications proxy
var tmMessage = {
    // type: info, success, warning, error
    // title is optional, add to the params list if required somewhere
    show: function(message, title, type) {
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "0",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };

        if (typeof type == 'undefined') {
            type = 'info';
        }
        if (type == 'info' || type == 'success') {
            // it is ok to autohide success messages
            toastr.options.timeOut = 5000;
        }
        toastr[type](message, title);
    },
    success: function(message, title) {
        this.show(message, title, 'success');
    },
    warning: function(message, title) {
        this.show(message, title, 'warning');
    },
    error: function(message, title) {
        this.show(message, title, 'error');
    },

    setFlash: function(message, type) {

    },
    showFlash: function() {

    }
};