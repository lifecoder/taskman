"use strict";

var tmProjects = angular.module('tmProjects', ['ngResource']);

tmProjects.factory('Project', ['$resource', function($resource) {
    return $resource('/api/projects/:id', {id:'@id'}, {
        update: {method: 'PUT'}
    });
}]);

tmProjects.controller('ProjectController', function($scope, Project) {
    $scope.projects = Project.query({}, function(result) {
        $scope.projects = result;
    });
});


tmProjects.controller('ProjectEditController', function($scope, $location, $routeParams, Project) {
    if ($routeParams.id) {
        $scope.project = Project.get({id: $routeParams.id});
    } else {
        $scope.project = new Project();
    }
    $scope.save = function() {
        if ($scope.project.id) {
            $scope.project.$update($scope.handleUpdate);
        } else {
            $scope.project.$save($scope.handleCreate);
        }
    };

    $scope.handleUpdate = function(result) {
        $scope.handleSave(result, 'Project updated');
    };

    $scope.handleCreate = function(result) {
        $scope.handleSave(result, 'Project created');
    };

    $scope.handleSave = function(result, message) {
        if (typeof result.errors == 'undefined') {
            tmMessage.success(message);
            $location.path("/dashboard");
        }
    }
});

