class ProjectsController < InheritedResources::Base
  load_and_authorize_resource
  respond_to :json
  before_filter :json_format

  rescue_from CanCan::AccessDenied do |exception|
    render json: {errors: [exception.message]}, status: 304
  end

  def create
    @project = build_resource
    @project.user = current_user

    if @project.save
      render json: @project
    else
      render json: { message: 'Validation failed', errors: @project.errors.full_messages }, status: 422
    end
  end

private

  def project_params
    params.permit(:name)
    # params.require(:project).permit(:name)
  end
end

