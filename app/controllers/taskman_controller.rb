class TaskmanController < ApplicationController
  def index
    if user_signed_in?
      flash.keep
      redirect_to app_path
    end
  end

  def app
  end
end
